# Infomaniak Cloud:  tutorials

*:warning: Disclamer: these tutorials are not managed by Infomaniak and are not official. To see the official documentation go here: https://docs.infomaniak.cloud/ :warning:*

Table of contents: 

- [Create a vm with public ip and private network](simple-instance-and-internal-network.md)

- [Create a kubernetes cluster with terraform and kubespray](k8s-cluster-with-terraform-and-kubespray.md)

