# Create a kubernetes cluster with terraform and kubespray

*:warning: Disclamer: these tutorials are not managed by Infomaniak and are not official. To see the official documentation go here: https://docs.infomaniak.cloud/ :warning:*

Intro
-----

Bootstraps a full featured kubernetes cluster using:
- [Infomaniak Public Cloud](https://www.infomaniak.com/en/hosting/public-cloud)
- [kubespray](https://kubespray.io/#/)
- [terraform](https://www.terraform.io/)

It will create a 1 master 3 worker nodes cluster with:
- [cri-o](https://cri-o.io/) container engine
- [cilium](https://cilium.io/) CNI
- openstack external [cloud provider](https://github.com/kubernetes/cloud-provider-openstack)

using only one floating address for the master and API access

Persistent volumes are provided by cinder and cinder-csi plugin

Services of type Load balancer are provided by octavia

It takes approximately 50 seconds to create the infrastructure with terraform,
and an additional 5 minutes to bootstrap the cluster.



## Preparation

### Get you openstack credentials 

From you openstack user administration page, click on `Download your Openstack file` to get your openstack credentials. 

![](res/openrc.1.png)

Then source them: 

```bash
source openstack_config.txt
```

### Install the requirements 

Set cluster name

```shell
export CLUSTER=mycluster
```

Create an ssh key and launch an agent

```shell
ssh-keygen -C "kubespray-$CLUSTER" -f $PWD/sshkey -t ed25519 -N ""
eval $(ssh-agent)
ssh-add sshkey
```

Prepare python virtual env

```shell
python3 -m venv venv
. venv/bin/activate
pip install -U pip wheel
```

Install terraform

```shell
mkdir bin
curl -L -o terraform_1.0.8_linux_amd64.zip https://releases.hashicorp.com/terraform/1.0.8/terraform_1.0.8_linux_amd64.zip
cd bin
unzip ../terraform_1.0.8_linux_amd64.zip
cd ..
export PATH=$PWD/bin:$PATH
```

Install kubectl

```shell
curl -L -o bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x bin/kubectl
```

Download kubespray

```shell
git clone --branch release-2.17 https://github.com/kubernetes-sigs/kubespray kubespray
cd kubespray
pip install -r requirements.txt
```

Install the openstack client

```bash
pip install openstackclient
```

Test your openstack credentials

```bash
openstack project list
```



## create infrastructure (terraform)

##### Overview

We will create the infrastructure with the openstack terraform module from kubespray:  https://github.com/kubernetes-sigs/kubespray/tree/v2.17.0/contrib/terraform/openstack

##### Prepare the inventory

```shell
# from the kubespray folder
mkdir inventory/$CLUSTER
cd inventory/$CLUSTER
export TERRAFORM_STATE_ROOT=$PWD
terraform init -from-module=../../contrib/terraform/openstack
cp sample-inventory/cluster.tfvars .
rm -r sample-inventory/
cp -r ../sample/group_vars/ group_vars
ln -sf ../../contrib/terraform/openstack/hosts
```

##### Edit the cluster.tfvars

Place the file *files/kubespray-terraform/cluster.tfvars* in your current directory. It should be located around here: `kubespray/inventory/$CLUSTER`

```bash
cp locationOfclustes.tfvars cluster.tfvars
```

[here](files/kubespray-terraform/cluster.tfvars) is an example. Edit the parameters you want. 

:warning: The following parameters have to be set! :warning:

public_key_path: the path to your public key created previously

```bash
PUBKEY_PATH=$(realpath ../../../sshkey.pub) && ls $PUBKEY_PATH
```

```bash
sed -i "s#public_key_path = .*\$#public_key_path = \"$PUBKEY_PATH\" #" cluster.tfvars && grep public_key_path cluster.tfvars 
```

image : instance image to use. (Use a recent image)

```bash
# list the available images
openstack image list

# here we will use bullseye. If you change the distribution you have to change the ssh_user user too. (the user used to ssh with)
IMAGE=$(openstack image list | grep -i bullseye | awk -F '|' '{print $3}' | xargs  ) && echo $IMAGE

sed -i "s/image = .*\$/image = \"$IMAGE\" /" cluster.tfvars && grep image cluster.tfvars 
```

k8s_allowed_remote_ips: The ips that will be allowed to connect via ssh.  You'll be able to modify this trough the security groups of the instance. 

```bash
ALLOWED_IP='"0.0.0.0/0"'

# Use this if you want allow only your ip: 
# ALLOWED_IP=$(curl 'https://api.ipify.org?format=json' -s | jq .ip ) && echo $ALLOWED_IP
```

```bash
sed -i "s#k8s_allowed_remote_ips = .*\$#k8s_allowed_remote_ips = [$ALLOWED_IP] #" cluster.tfvars && grep k8s_allowed_remote_ips cluster.tfvars 
```





:information_source: The following parameters can be leaved as they are in the `cluster.tfvars`.  You can jump to the `Create the infrastructure` section. :information_source:



cluster name: 

```bash
sed -i "s/cluster_name = .*\$/cluster_name = \"$CLUSTER\" /" cluster.tfvars && grep cluster_name cluster.tfvars 
```

flavor_k8s_master: 

```bash
# list the available flavor 
openstack flavor list

# we will use this one for the master:  a2-ram4-disk80-perf1
FLAVOR=$(openstack flavor list -f value | grep a2-ram4-disk80-perf1 | awk '{print $1}') && echo $FLAVOR
```

```bash
sed -i "s/flavor_k8s_master = .*\$/flavor_k8s_master = \"$FLAVOR\" /" cluster.tfvars && grep flavor_k8s_master cluster.tfvars 
```

flavor_k8s_node: 

```bash
# list the available flavor
openstack flavor list

# we will use this one for the nodes: a4-ram8-disk80-perf1
FLAVOR=$(openstack flavor list -f value | grep a4-ram8-disk80-perf1 | awk '{print $1}') && echo $FLAVOR
```

```bash
sed -i "s/flavor_k8s_node = .*\$/flavor_k8s_node = \"$FLAVOR\" /" cluster.tfvars && grep flavor_k8s_node cluster.tfvars 
```

network_name. The internal network name we will create. 

```bash
sed -i "s/network_name = .*\$/network_name = \"$CLUSTER\" /" cluster.tfvars && grep network_name cluster.tfvars 
```

external_net: the external network 

```bash
# list the networks 
openstack network list

# Should use this network for floating ips
NETWORK=$(openstack network list --name ext-floating1 -f value -c ID) && echo $NETWORK
```

```bash
sed -i "s/external_net = .*\$/external_net = \"$NETWORK\" /" cluster.tfvars && grep external_net cluster.tfvars 
```

floatingip_pool: the pool where to pick the floating ips

```bash
# list the networks 
openstack network list

# should use this one: ext-floating1 leave it as it is
```



##### Create the infrastructure

```shell
terraform apply -var-file=cluster.tfvars
```

You should then have your 3 nodes + 1 master running :blush:

```bash
openstack server list -c Name -c State -c Networks -c Image -c Flavor
+-------------------------+---------------------------------------+----------------------+----------------------+
| Name                    | Networks                              | Image                | Flavor               |
+-------------------------+---------------------------------------+----------------------+----------------------+
| mycluster-k8s-master-1  | mycluster=10.10.0.205, 195.15.***.*** | Debian 11.1 bullseye | a2-ram4-disk80-perf1 |
| mycluster-k8s-node-nf-1 | mycluster=10.10.0.86                  | Debian 11.1 bullseye | a4-ram8-disk80-perf1 |
| mycluster-k8s-node-nf-3 | mycluster=10.10.0.68                  | Debian 11.1 bullseye | a4-ram8-disk80-perf1 |
| mycluster-k8s-node-nf-2 | mycluster=10.10.0.159                 | Debian 11.1 bullseye | a4-ram8-disk80-perf1 |
+-------------------------+---------------------------------------+----------------------+----------------------+
```



## kubespray

### Adapt the ansible vars

On the same folder as the previous chapter (it should be:  ``kubespray/inventory/$CLUSTER`) , get the output of the newly created lb subnets

```bash
FLOATING_NETWORK_ID=$( terraform output --json | jq -r .floating_network_id.value  ) && echo $FLOATING_NETWORK_ID
```

```bash
PRIVATE_SUBNET_ID=$( terraform output --json | jq -r .private_subnet_id.value  ) && echo $PRIVATE_SUBNET_ID
```

Get the group_vars.diff

```bash
cp locationOfYouGroup_vars.diff . 
```

 adapt the `group_vars` with the following or with this [patch file](files/kubespray-terraform/group_vars.diff)

```bash
sed -i "s/FLOATING_NETWORK_ID/$FLOATING_NETWORK_ID/g" group_vars.diff && grep $FLOATING_NETWORK_ID group_vars.diff
```

```bash
sed -i "s/PRIVATE_SUBNET_ID/$PRIVATE_SUBNET_ID/g" group_vars.diff && grep $PRIVATE_SUBNET_ID group_vars.diff
```

```shell
patch -p0 < group_vars.diff
```



:information_source: an example of what you should see on these files. To continue procedure go to install k8s using kubespray :information_source:

###### all/all.yml

We set the `Infomaniak dns servers`, and set our `openstack cloud_provider`

```diff
-etcd_kubeadm_enabled: false
+etcd_kubeadm_enabled: true

+upstream_dns_servers:
+  - 83.166.143.51
+  - 83.166.143.52

+nameservers:
+  - 83.166.143.51
+  - 83.166.143.52

+cloud_provider: external
+external_cloud_provider: openstack

+download_container: false

+download_keep_remote_cache: true

```

###### all/cri-o.yml

```yaml
---
crio_registries_mirrors:
  - prefix: docker.io
    insecure: false
    blocked: false
    location: registry-1.docker.io
    mirrors:
      - location: mirror.gcr.io
        insecure: false

crio_pids_limit: 4096
```

###### all/openstack.yml

```yaml
---
external_openstack_lbaas_subnet_id: <- private_subnet_id from terraform output
external_openstack_lbaas_floating_network_id: <- floating_network_id from terraform output
cinder_csi_enabled: true
cinder_csi_ignore_volume_az: true
```

###### k8s_cluster/k8s-cluster.yml

```diff
-kube_network_plugin: calico
+kube_network_plugin: cilium

-resolvconf_mode: docker_dns
+resolvconf_mode: host_resolvconf

-container_manager: docker
+container_manager: crio

+kubeconfig_localhost: true

-persistent_volumes_enabled: false
+persistent_volumes_enabled: true
+expand_persistent_volumes: true
```



### Install k8s using the kubespray ansible playbook

go back to your `kubespray` folder

```shell
cd ../..
```

Install mitogen to speed things up. (if you have multiple ansible installations, you may want to use the one installed in venv: `realpath ../venv/bin/ansible-playbook ` )

```shell
ansible-playbook mitogen.yml
```

Test connectivity to all nodes

```shell
ansible -i inventory/$CLUSTER/hosts -m ping all
```



> if it fails, check if you have set the correct public key: 
>
> ```bash
> ssh debian@195.15.***.*** -i ../sshkey
> ```
>
> then check if you can ping specifiying the ssh key. If so, you should adapt your ssh agent. Or run the playbook with the `--key-file` param. 
>
> ```bash
> ansible -i inventory/$CLUSTER/hosts -m ping all -u debian --key-file ../sshkey
> ```
>
> 



> you also can update your ssh config in order to connect to the bastion :
>
> ```bash
> Host mycluster-k8s-node-nf-*
>     IdentityFile /location/to/your/sshkey
>     ProxyCommand ssh 195.15.***.*** -W %h:%p
>     User debian
> Host mycluster-k8s-node-nf-1
>     Hostname 10.10.0.11
> Host mycluster-k8s-node-nf-2
>     Hostname 10.10.0.67
> Host mycluster-k8s-node-nf-3
>     Hostname 10.10.0.135  
> Host 195.15.***.***
>     IdentityFile /location/to/your/sshkey
>     User debian
> ```
>
> get the ips from: 
>
> ```bash
> openstack server list
> ```



Install the cluster

```shell
ansible-playbook --become -i inventory/$CLUSTER/hosts cluster.yml
```



## end-to-end test

export your kubeconfig

```shell
export KUBECONFIG=$PWD/inventory/$CLUSTER/artifacts/admin.conf
```

Create a pod a try to consume kubernetes API.

This validates pod creations, network connectivity and API

```shell
kubectl run -ti --rm --restart=Never --overrides='{"spec": { "terminationGracePeriodSeconds" :0 } }' toolbox --image=ghcr.io/reneluria/alpinebox:1.3.2 -- bash -c 'curl --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt -H "Authorization: Bearer $(</var/run/secrets/kubernetes.io/serviceaccount/token)" https://kubernetes.default/api'
```

That's all, you can use the *kubeconfig* file at `inventory/$CLUSTER/artifacts/admin.conf`



## Usage

### Overview

```bash
$ kubectl get pods -A
NAMESPACE     NAME                                             READY   STATUS    RESTARTS   AGE
[...]
kube-system   csi-cinder-controllerplugin-99c9dd87b-8b2jn      5/5     Running   0          16m
kube-system   csi-cinder-nodeplugin-8fr7t                      2/2     Running   0          16m
kube-system   csi-cinder-nodeplugin-gdcc6                      2/2     Running   0          16m
kube-system   csi-cinder-nodeplugin-t4z2x                      2/2     Running   0          16m
[...]
kube-system   nginx-proxy-mycluster-k8s-node-nf-1              1/1     Running   0          35m
kube-system   nginx-proxy-mycluster-k8s-node-nf-2              1/1     Running   0          35m
kube-system   nginx-proxy-mycluster-k8s-node-nf-3              1/1     Running   0          35m
[...]
kube-system   openstack-cloud-controller-manager-29csn         1/1     Running   0          16m
```

The `csi-cinder-*` pods are the ones managing the k8s volumes created by openstack.  Its configuration can be found in the `cloud-config` secret, where you will find the `OS_*` env variables given at the installation: 

```bash
kubectl get secret cloud-config  -n kube-system -o json | jq -r '.data."cloud.conf"' | base64 -d
```

The `openstack-cloud-controller-manager` pod is the one managing openstack resources like loadbalancers. Its configuration can be found in the `external-openstack-cloud-config` secret. It contains the `OS_*` env variables givent at the installation, and some subnets id where to pick floating ips. 

```bash
kubectl get secret external-openstack-cloud-config  -n kube-system -o json | jq -r '.data."cloud.conf"' | base64 -d
```

