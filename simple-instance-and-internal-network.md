# Simple instance and internal network

*:warning: Disclamer: these tutorials are not managed by Infomaniak and are not official. To see the official documentation go here: https://docs.infomaniak.cloud/ :warning:*

### Overview

This guide covers the basics of Openstack and shows how to create a instance accessible trough ssh. The instance will be connected to Internet trough a public ip, and will also be connected to a private network where we will be able to create others instances. 

```mermaid
graph LR
subgraph net[internal network: mycluster]
instance2 --> instance1
instance1[my-instance] <-->|only accessible from internal network| instance2[my-instance-2]
end
internet((Internet)) -->|publicly accessible trough ssh| instance1
```

This is done in 6 steps: 

1. We create private network named `mycluster` with this subnet: `10.10.0.0/24`. These will be the default ips allocated to our instances. 
2. We create a router connected to Internet named `my-router`, and associate it to our private network. This way, our instances will have Internet access. 
3. Allow ssh and icmp (ping) by opening ports trough `security groups`. This will permit ssh traffic to reach our instance via ssh. 
4. Create an `ssh keypair`. We will use this ssh key to connect to our instances. Openstack will place the public part on our instances. 
5. Create our instances in our `internal network`
6. Assign a public floating ip to `my-instance` in order to access it from Internet. 



We explain 2 ways of doing this: 

- [from our web dashboard. (Horizon dashboard)](#dashboard-guide)
- [trough CLI](#cli-guide)

## Dashboard Guide <a id="dashboard-guide"></a>

1. [Create network](#dashboard-guide-network)
2. [Create a router](#dashboard-guide-router)
3. [Create security groups](#dashboard-guide-secgroups)
4. [Create an ssh keypair](#dashboard-guide-keypair)
5. [Create our instances](#dashboard-guide-instance)
6. [Assign a floating ip](#dashboard-guide-floatingip)
7. [Create other instances and access them on the private network](#dashboard-guide-sshagent)

### Create the internal network <a id="dashboard-guide-network"></a>

Go to  `Network > Networks > Create Network`: 

![](res/network.create.1.png)

Choose a name and network range for our internal network. 

![](res/network.create.2.png)

![](res/network.create.3.png)

We place the Infomaniak's names server. 

The click on `Create`

### Create the router <a id="dashboard-guide-router"></a>

Now, we create a router to give Internet access to our private network. 

Go to `Network > Routers > Create Router`

![](res/router.create.1.png)

Select the external network `ext-floating1`, then click `Create Router`

Then go to `Network > Network Topology`, and we see that our router is connected to the public network`ext-floating1`, which is connected to Internet.   :earth_americas:

To link the router to our internal network, click on it, then `Add Interface`

![](res/router.interface.1.png )

![](res/router.interface.2.png)

On the interface, select our subnet (the internal network), then click on `Submit`. 

We now see that our subnet `mycluster-internal-network` is conencted to the public network `ext-floating1` trough our router. 

![](res/router.interface.3.png)



### Allow ssh and icmp traffic <a id="dashboard-guide-secgroup"></a>

By default, all incoming traffic is blocked by Openstack. To open it, we create `Security Groups`.  Go to `Network > Security Groups > Create Security Group`. 

##### Allow ssh: 

Name our security group `secgroup-ssh`, then click `Create Security Group`. 

![](res/secgroup.ssh.1.png)

The click on `Manage Rules` and open the port 22: 

![](res/secgroup.ssh.2.png)

##### Allow icmp (ping): 

Do the same with the security group `secgroup-icmp`: 

![](res/secgroup.icmp.png)

![](res/secgroup.icmp.2.png)



### Create an ssh key-pair <a id="dashboard-guide-ssh-keypair"></a>

In order to connect trough ssh to our instances, we create an ssh keypair. The public part will be placed on the instance by Openstack, and will use the private part to connect trough ssh. 

Go to `Compute > Key Pairs > Create Key Pair`

![](res/keypair.1.png)

The private part will be downloaded. Place it here for example: 

```bash
$ cat ~/.ssh/infomaniak-cloud-key 
-----BEGIN RSA PRIVATE KEY-----   
[...]
```

And set the correct permissions on it

```bash
$ chmod 700  ~/.ssh/infomaniak-cloud-key
```



### Create an instance <a id="dashboard-guide-instance"></a>

Now, we have every thing to create our instance! Go to `Compute > Instances > Launch Instance`

![](res/instance.create.1.png)

Select the image you want. Here we select Debian 11. 

![](res/instance.create.2.png)

Select the flavor you want. 

![](res/instance.create.3.png)

Place the instance in our private network we created previously. 

![](res/instance.create.4.png)

Assign our 2 security groups to the instances so we can ping it and access it trough ssh. 

![](res/instance.create.5.png)

Select the ssh key you want to use to connect trough ssh. 

![](res/instance.create.6.png)

The click on `Launch Instance`

We can now see our instance up, and openstack assigned a private ip to it. (`10.10.0.98`).  

![](res/instance.created.ip.0.png)

But since the instance is on a private network, we have not access to it already. We have to associate a public floating ip to our get to our instance. 

```bash
$ ping 10.10.0.98
PING 10.10.0.98 (10.10.0.98) 56(84) octets de données.
^C
--- statistiques ping 10.10.0.98 ---
6 paquets transmis, 0 reçus, 100% packet loss, time 5075ms

```



### Associate a floating ip to our public instance <a id="dashboard-guide-floatingip"></a>

To give a public ip to our instance, select `Associate Floating IP`: 

![](res/instance.floating.ip.png)

Click on the :heavy_plus_sign: sign to allocate a new ip: 

![](res/instance.floating.ip.2.png)

Select the `ext-floating` pool of ips, then click on `Allocate IP` to receive an IP. 

![](res/instance.floating.ip.3.png)

After that, our instance received a public ip and is now pingable!

![](res/instance.created.ip.png)

```bash
$ ping 195.15.***.***             
PING 195.15.***.*** (195.15.***.***) 56(84) octets de données.
64 octets de 195.15.***.*** : icmp_seq=1 ttl=50 temps=7.33 ms
^C
--- statistiques ping 195.15.***.*** ---
1 paquets transmis, 1 reçus, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 7.333/7.333/7.333/0.000 ms
```

We can ssh to it like that: 

```bash
ssh debian@195.15.***.*** -i ~/.ssh/infomaniak-cloud-key
[...]
debian@my-instance:~$ 

```



### Create other instances on private network and access to them via ssh <a id="dashboard-guide-sshagent"></a>

To create other instances, simply follow the [Create instance step](#dashboard-guide-instance) without associating a public floating ip. In that way, they will be protected from Internet since there wont be accessible from it. The only way to ssh it will be trough our instance with a public ip. 

For example, here we have  `myinstance` connected having a public ip, and `myinstance-2` that is protected in our internal network. In this configuration, `myinstance` is named a `bastion`. 

![](res/instance.created.ip.2.png)

One *(not recommended)* way to connect to `my-instance-2` would be connect trough ssh on `my-instance`, then place our private key on it in order to ssh to `my-instance-2` from it.  

The other way would be to start an `ssh-agent` in with we will place our private key. Then forwarding the agent when we connect to ssh in order to "*take our private key with us*": 

```bash
# check if there is any ssh agent running
$ ps -p $SSH_AGENT_PID 2>/dev/null || echo not running            
not running

# start the ssh agent 
$ eval $(ssh-agent)
Agent pid 18539

# add our key to the agent 
$ ssh-add ~/.ssh/infomaniak-cloud-key 
Identity added: ~/.ssh/infomaniak-cloud-key (~/.ssh/infomaniak-cloud-key)

# we can see that our agent has our key
$ ssh-add -l
2048 SHA256:ZFjkuwhifDAoAtK5Z2kA8mClrfevt4d0qWX0PG3HRoA ~/.ssh/infomaniak-cloud-key (RSA)

# we can now connect to my-instance with -A to forward our agent
ssh -A debian@195.15.***.*** -i ~/.ssh/infomaniak-cloud-key
[...]
debian@my-instance:~$ 

# and from my-instance go to my-instance-2 
debian@my-instance:~$ ssh -A debian@10.10.0.143
[...]
debian@my-instance-2:~$ 
```



If you want to connect  automatically to `my-instance-2` without having to jump on `my-instance` manually, you can set your `ssh config` as described here for example: https://blog.scottlowe.org/2017/05/26/bastion-hosts-custom-ssh-configs/



## CLI Guide <a id="cli-guide"></a>

1. [Install the openstack client](#cli-guide-install)
2. [Get you openstack credentials](#cli-guide-openrc)
3. [Create network](#cli-guide-network)
4. [Create a router](#cli-guide-router)
5. [Create security groups](#cli-guide-secgroups)
6. [Create an ssh keypair](#cli-guide-keypair)
7. [Create our instances](#cli-guide-instance)
8. [Assign a floating ip](#cli-guide-floatingip)
9. [Create other instances and access them on the private network](#cli-guide-sshagent)



### Install the openstack client<a id="cli-guide-install"></a>

Prepare the python virtual env

```bash
python3 -m venv venv
. venv/bin/activate
pip install -U pip wheel
```

Install the client

```bash
pip install openstackclient
```



### Get you openstack credentials <a id="cli-guide-openrc"></a>

From you openstack user administration page, click on `Download your Openstack file` to get your openstack credentials. 

![](res/openrc.1.png)

Then source them: 

```bash
source openstack_config.txt
```

And check that your credentials are good with this command: 

```bash
openstack project list
```



### Create the network <a id="cli-guide-network"></a>

We create  a `10.10.0.0/24` internal network 

```bash
export NETWORK_NAME=mycluster
openstack network create $NETWORK_NAME
```



```bash
openstack subnet create --network $NETWORK_NAME --subnet-range 10.10.0.0/24 --dns-nameserver 83.166.143.51 --dns-nameserver 83.166.143.52 $NETWORK_NAME-internal-network
```



### Create the router <a id="cli-guide-router"></a>

In order to give Internet to the machines on this private network, we create a router and link it to the public network. 

```bash
openstack router create $NETWORK_NAME-router
```

We link it to Internet

```bash
# to see the networks
openstack network list 

# we link the router to the public network
openstack router set --external-gateway  $NETWORK_NAME-router
```

And link it to our private network

```bash
# to see the networks
openstack network list 

openstack router add subnet $NETWORK_NAME-router $NETWORK_NAME-internal-network 
```



### Allow ssh and icmp traffic <a id="cli-guide-secgroups"></a>

By default, all incoming traffic is blocked by Openstack. To open it, we create `Security Groups`.

##### Allow ssh

```bash
# create the sec group
openstack security group create secgroup-ssh

# add a rule 
openstack security group rule create --protocol tcp  --dst-port 22 --remote-ip 0.0.0.0/0 secgroup-ssh

# check: 
openstack security group rule list secgroup-ssh
```

##### Allow icmp (ping)

```bash
# create the sec group
openstack security group create secgroup-icmp

# add a rule 
openstack security group rule create --protocol icmp --remote-ip 0.0.0.0/0 secgroup-icmp

# check: 
openstack security group rule list secgroup-icmp
```



### Create an ssh key-pair <a id="cli-guide-keypair"></a>

In order to connect trough ssh, we create an ssh keypair. The public part will be
placed on the instance by Openstack, and will use the private part to connect trough ssh.

```bash
# this creates a key pair and places the private part to our .ssh folder
openstack keypair create my-ssh-key | tee ~/.ssh/infomaniak-cloud-key

# set the correct permissions
chmod 700 ~/.ssh/infomaniak-cloud-key
```



### Create an instance <a id="cli-guide-instance"></a>

```bash
# choose your image
openstack image list
IMAGE=$(openstack image list | grep -i bullseye | awk -F '|' '{print $2}' | tr -d ' ' ) && echo $IMAGE

# choose you flavor
openstack flavor list
FLAVOR=a1-ram2-disk50-perf1

# create the instance 
openstack server create --image $IMAGE --flavor $FLAVOR --network $NETWORK_NAME --key-name my-ssh-key --security-group secgroup-ssh --security-group secgroup-icmp  my-instance

# check the instance after some seconds
openstack server show  my-instance
```

We can now see our instance up, and openstack assigned a private ip to it. ( 10.10.0.161 ).

```bash
openstack server show my-instance                                                                   
+-----------------------------+-------------------------------------------------------------+
| Field                       | Value                                                       |
+-----------------------------+-------------------------------------------------------------+
| OS-DCF:diskConfig           | MANUAL                                                      |
| OS-EXT-AZ:availability_zone | dc3-a-10                                                    |
| OS-EXT-STS:power_state      | Running                                                     |
| OS-EXT-STS:task_state       | None                                                        |
| OS-EXT-STS:vm_state         | active                                                      |
| OS-SRV-USG:launched_at      | 2021-10-16T14:34:37.000000                                  |
| OS-SRV-USG:terminated_at    | None                                                        |
| accessIPv4                  |                                                             |
| accessIPv6                  |                                                             |
| addresses                   | mycluster=10.10.0.161                                       |
| config_drive                |                                                             |
| created                     | 2021-10-16T14:34:29Z                                        |
| flavor                      | a1-ram2-disk50-perf1 (366010f9-67e0-4c83-bae0-e3bdec3c2c5d) |
| hostId                      | 2b401223a45ed66f17287c7943ffcedc12125a50a2c8b4a9f3689c38    |
| id                          | d48dc711-693b-414c-a531-f5d8d6bf1160                        |
| image                       | Debian 11.1 bullseye (3ef22971-0f9b-4244-9c45-080bc316d3aa) |
| key_name                    | my-ssh-key                                                  |
| name                        | my-instance                                                 |                              
| progress                    | 0                                                           |                                
| project_id                  | 91f09c6c9c66412ca57e3fb48493f944                            |
| properties                  |                                                             |
| security_groups             | name='secgroup-ssh'                                         |
|                             | name='secgroup-icmp'                                        |
| status                      | ACTIVE                                                      |                                
| updated                     | 2021-10-16T14:34:37Z                                        |
| user_id                     | 6787d1e6fad6443aa8081566244ed186                            |
| volumes_attached            |                                                             |
+-----------------------------+-------------------------------------------------------------+

```

But since the instance is on a private network, we have not access to it already. We have to
associate a public floating ip to our get to our instance.

```bash 
$ ping 10.10.0.161
PING 10.10.0.161 (10.10.0.161) 56(84) octets de données.
^C
--- statistiques ping 10.10.0.161 ---
2 paquets transmis, 0 reçus, 100% packet loss, time 1009ms
```



### Associate a floating ip to our public instance <a id="cli-guide-floatingip"></a>

To be able to access our instance, we have to associate it a public floating ip

```bash
# list the availables network
openstack network list

# create a floating ip from the network ext-floating1
openstack floating ip create  ext-floating1

# get the created floating ip
openstack floating ip list

# associate the floating ip to our instance
openstack server add floating ip my-instance 195.15.***.***
```



Once the ip is affected, we can ssh to the instance: 

```bash
$ ping 195.15.***.***             
PING 195.15.***.*** (195.15.***.***) 56(84) octets de données.
64 octets de 195.15.***.*** : icmp_seq=1 ttl=50 temps=7.33 ms
^C
--- statistiques ping 195.15.***.*** ---
1 paquets transmis, 1 reçus, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 7.333/7.333/7.333/0.000 ms
```

We can ssh to it like that: 

```bash
ssh debian@195.15.***.*** -i ~/.ssh/infomaniak-cloud-key
[...]
debian@my-instance:~$ 

```



### Create other instances on private network and access to them via ssh <a id="cli-guide-sshagent"></a>

See [this step](#dashboard-guide-sshagent)
